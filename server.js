const express = require('express');
const mongoose =  require('mongoose');

// DB config
const mongodb = require('./config/keys').mongoURI;

// mongdb connect
mongoose.connect(mongodb)
.then(()=>console.log("mongodb Connected"))
.catch((err)=>console.log("Error connecting mongodb:", err))

// initiliaze express variable
const app = express();
app.get('/',(req, res)=>res.send('Hello world To Onboarding Process by Globant'));

const port = process.env.PORT || 5000;

app.listen(port,()=>console.log("App listening at port:", port))
